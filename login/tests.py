from django.test import TestCase,Client
from .views import login

from selenium import webdriver
import unittest
import time


# Create your tests here.
class UnitTest(TestCase):
    #Test untuk cek url login
    def test_url_address_login_is_exist(self):
        response = Client().get('/login/login_account/')
        self.assertEqual(response.status_code, 200)

    #Test untuk cek alamat url login tidak ada
    def test_url_address_login_is_not_exist(self):
        response = Client().get('/')
        self.assertNotEqual(response.status_code, 200)

    #Test untuk cek fungsi login telah memiliki isi
    def test_login_is_written(self):
        self.assertIsNotNone(login)

    #Test untuk cek template html telah digunakan pada halaman login
    def test_template_html_login_is_exist(self):
        response = Client().get('/login/login_account/')
        self.assertTemplateUsed(response, 'login.html')

class FunctionalTest(unittest.TestCase):
    #Melakukan setUp untuk starting web browser, yaitu dengan menggunakan google chrome
    def setUp(self):
        self.browser = webdriver.Chrome()

    #Melakukan tearDown dengan mengeluarkan web browser
    def tearDown(self):
        self.browser.implicitly_wait(3)
        self.browser.quit()

    #Melakukan test functional ke website yang telah di deploy
    def test_functional(self):
        self.browser.get('https://maisy-websitestory9ppw.herokuapp.com/login/login_account/')
        time.sleep(5)
        username_box = self.browser.find_element_by_name('uname')
        username_box.send_keys('ppw2019')
        password_box = self.browser.find_element_by_name('pass')
        password_box.send_keys('pepewisthebest')
        buttonLogin = self.browser.find_element_by_id('submit-login')
        buttonLogin.submit()
        time.sleep(10)
        self.assertIn('Login', self.browser.page_source)

if __name__ == '__main__':
   unittest.main(warnings='ignore')
