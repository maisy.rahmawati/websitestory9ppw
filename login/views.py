from django.shortcuts import render
from django.urls import reverse
from django.http import HttpResponse
from django.contrib import messages
from django.contrib.auth import authenticate
from django.http import HttpResponseRedirect

# Create your views here.

def login(request):
    if request.method == "POST":
        user = authenticate (username = request.POST.get('username'), password = request.POST.get('username'))
        if user is not None:
            login(request, user)
            request.session['username'] = username
            response['username'] = request.session['username']
            return redirect(reverse('login:login_account'))
            #return render(request, 'tampilan.html')
        else:
            messages.add_message(request, messages.ERROR, 'Error account')
    return render(request, 'login.html')

def tampilan(request):
    return render(request, 'tampilan.html')

def logout(request):
    request.session.flush()
    messages.add_message(request, messages.INFO, 'Successfull logout!')
    logout(request)
    return HttpResponseRedirect(reverse('login:login'))
    #return redirect('login/login_account/')

