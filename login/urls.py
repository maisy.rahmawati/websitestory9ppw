from django.conf.urls import url
from . import views
from django.urls import path

app_name = 'login'
urlpatterns = [
    path('login_account/', views.login, name = 'login_account'),
    path('tampilan/', views.tampilan, name = 'tampilan'),
    path('logout_account/', views.logout, name = 'logout_account'),
]
